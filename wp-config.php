<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpressdb');

/** MySQL database username */
define('DB_USER', 'wordpressuser');

/** MySQL database password */
define('DB_PASSWORD', 'wordpresspassword');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'vYk S+c(4:0C*lKqhnzjjqk?tMGrnUzHjDxrH:>,X1@*-Rq!+k^e^0,S-vgv@4HA');
define('SECURE_AUTH_KEY',  '*Xvjhl^;<Y`lBd$xN.PSj^Bc]+f/X>i]jMobPs|iq0:B.af42q%(}^8b;@zdc2.#');
define('LOGGED_IN_KEY',    'K&m+EW:1z=4xQ_F6,PwEUk@Yo4y?,T/9MeqZjf.;ylr1Q9g1WrQPDQitXrU-i 6M');
define('NONCE_KEY',        '<F+.i<a7W;=Z*:c 5o7R1^b#8dqt*UF4Wk;M7[V1k/3sb,]TsVY?MIm# p<s:2~5');
define('AUTH_SALT',        ':lJtE$HvP6.[0hm(q@!vaY=rnv)r2*[6O Ix>XEFPVE{k*bYvn7IMH7NVjYXXXy0');
define('SECURE_AUTH_SALT', 'Lf?e)j_@;/K4zR9hqS40!OCq{Hj(].?P(J3g;,tNj]N]TG>e<GneE:ucB6$qZk&0');
define('LOGGED_IN_SALT',   'cAyxfd[m?M)vUxezc_g&7j72F_5VEu6OiZdK^|[bEsItGx-d-]5lk+r:^L0p*Dd.');
define('NONCE_SALT',       'BD!8iITBXvNtOnaw~(e(*R2Z>s=<vb>RWCe<3jXsWK;s5uOOB3P3?<bCgQ~0AQ#y');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
