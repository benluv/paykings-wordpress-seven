# README #

This README file will help you install and run the wordpress project in your computer.

### What is this repository for? ###

* This is a project developed for Corp-PAYKINGS. The objective of this project is to showcase the abilities while creating a customizable theme from scratch
* Version 0.0.1

### How to get it set up and running ###

* Copy the link and clone it into your system. From here you will need to install the Apache server as well as PHP and MySQL. This can be easily done by running the apt-get install lamp-server^ command.
* You'll need to initiate the mysql server and create a new database, the give all privileges to the current user and the flush privileges. Afterwards, add the project inside the /var/www/html folder, be sure to grant permissions in this folder.
* The only dependency is gulp.js in order to better visualize changes. Jquery can be easily called inside the functions.php inside the 'function seven_scripts()' without the need of installing external packages.
* Open the terminal and browse to the themes/gulp-dev folder. Inside of this folder simply call the 'gulp' command in the terminal and another window in localhost should open and show the changes in the page realtime.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Be sure to contact me through my email - luongov13@gmail.com if you have any questions or would like to give some feedback.

### The Easy way to view it ###

* Simply browse to the theme folder, zip the 'seven' folder, log into your wordpress site, and add the folder into your predesigned themes.